"""
This python script is the starting point of this project:
Step 1: Use the methods in parse_pdb_and_create_suites.py to create a list of objects of the Suite-type (see
Suite_class.py).
Step 2: Use the methods in read_clash_files.py to extend the suites with information about atom-clashes.
Step 3: Use the methods in shape_analysis to extend the suites with information about the shape. I.e. use the procrustes
        algorithm to mean align the shapes and cluster the data.
Step 4: Parse the ERRASER files. And make statistics with the ERRASER data.
Step 5: Apply CLEAN (CLassification basEd on mutliscAle eNhancement) .
Step 6: Apply CLEAN to two examples of the Corona data set.
"""

from clash_correction import multiscale_correction
from corona_different_models import working_with_different_models
from parse_functions import parse_pdb_files, parse_clash_files, parse_erraser_files, shape_analysis_suites

# Starting point:
if __name__ == '__main__':
    string_folder = './out/saved_suite_lists/'
    # Step 1:
    suites = parse_pdb_files(input_string_folder=string_folder, input_pdb_folder=None)
    # Step 2:
    suites = parse_clash_files(input_suites=suites, input_string_folder=string_folder)
    # Step 2b: Not in the code at the moment:
    # suites = parse_base_pairs(input_suites=suites, input_string_folder=string_folder)
    # Step 3:
    suites = shape_analysis_suites(input_suites=suites, input_string_folder=string_folder)
    # Step 4:
    suites = parse_erraser_files(input_suites=suites, input_string_folder=string_folder)
    # Step 5:
    suites = multiscale_correction(input_suites=suites)
    # Step 6:
    working_with_different_models(input_suites=suites)

    print('test')
