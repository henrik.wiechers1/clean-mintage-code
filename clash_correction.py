import os

import numpy as np
from scipy.optimize import leastsq

from data_functions import rotation, distance_rotation, rotation_matrix_axis_angle, \
    procrustes_algorithm_short,rotate_y_optimal_to_x, calculate_angle_3_points
from help_plot_functions import plots_beginning_correction, plot_correction_results, plot_4_ring_correction, \
    plot_all_classes
from PNDS_PNS import torus_mean_and_var
from plot_functions import hist_own_plot


def multiscale_correction(input_suites):
    """
    The multiscale correction method described in the paper.
    :param input_suites: A list with all suites
    :return: A modified list with suites.
    """
    number_neighbors = 50
    plot_all = True #False
    complete_suites = plots_beginning_correction(input_suites)
    hist_own_plot([np.linalg.norm(complete_suite.mesoscopic_sugar_rings[3]-complete_suite.mesoscopic_sugar_rings[2]) for complete_suite in complete_suites],
                  x_label="Distance between two consecutive sugar rings", y_label="Number of tuples of two sugar rings",
                  filename="distances_sugar_rings", density=False)

    hist_own_plot([calculate_angle_3_points([complete_suite.mesoscopic_sugar_rings[2], complete_suite.mesoscopic_sugar_rings[3], complete_suite.mesoscopic_sugar_rings[4]]) for complete_suite in complete_suites],
                  x_label=r"Angle between three consecutive sugar rings", y_label="Number of tuples of three sugar rings",
                  filename="angle_sugar_rings", density=False)
    procrustes_data = np.array([suite.procrustes_complete_mesoscopic_vector for suite in complete_suites])
    dihedral_angles = [complete_suites[i].dihedral_angles for i in range(len(complete_suites))]
    procrustes_data_backbone = np.array([suite.procrustes_complete_suite_vector for suite in complete_suites])
    # The list of backbone backbone clashes:
    backbone_validation_list = [i for i in range(len(complete_suites)) if len(complete_suites[i].bb_bb_one_suite) > 0]
    # The list of suites with a clash inside the mesoscopic strand
    backbone_validation_list_2 = [i for i in range(len(complete_suites)) if len(complete_suites[i].bb_bb_one_suite) > 0 or len(complete_suites[i].bb_bb_neighbour_clashes) > 0]

    string_ = 'suite_True'
    folder = './out/multiscale_correction/'
    if not os.path.exists(folder):
        os.makedirs(folder)
    # The cluster lists or 'reference  classes'
    cluster_list_backbone = create_cluster_list(complete_suites, string_)
    len_cluster_list_backbone = [len(cluster_list_backbone[i]) for i in range(len(cluster_list_backbone))]
    #plot_all_classes(cluster_list_backbone, dihedral_angles, backbone_validation_list_2, folder)
    # Some list for plotting the results:
    erraser_corrections = []
    all_distances = []
    all_single_distances = []
    all_distances_resolution = []
    all_resolutions = []
    all_nr_in_cluster = []
    for k in range(len(backbone_validation_list)):
        clash_suite = backbone_validation_list[k]
        data = procrustes_data.copy()
        x = procrustes_data[clash_suite].copy()
        for i in range(len(data)):
            data[i] = rotate_y_optimal_to_x(x=x, y=data[i])
        neighbors_dummy = np.array([np.linalg.norm(data[clash_suite] - data[element]) for element in
                                        range(data.shape[0])]).argsort()[1:]
        neighbors = [neighbors_dummy[i] for i in range(len(neighbors_dummy)) if
                         neighbors_dummy[i] not in backbone_validation_list_2][0:number_neighbors]

        nr_in_cluster = [len(set(cluster) & set(neighbors)) for cluster in cluster_list_backbone]
        nr_in_cluster_relative = [nr_in_cluster[i]/len_cluster_list_backbone[i] if nr_in_cluster[i]>number_neighbors/10 else 0 for i in range(len(nr_in_cluster))]
        #nr_in_cluster_relative = [nr_in_cluster[i]/len_cluster_list_backbone[i] for i in range(len(nr_in_cluster))]
        all_nr_in_cluster.append(np.max(nr_in_cluster))

        print(np.max(nr_in_cluster)/number_neighbors)
        if np.max(nr_in_cluster) > 1: #/number_neighbors >= 0.2:

            meso_shift_shape, neighbors_cluster, neighbour_mean, mesoscopic_clash_shape_rotated, procrustes_data_small_rotated = orthogonal_projection_on_M_c(clash_suite, cluster_list_backbone, neighbors, nr_in_cluster, procrustes_data, procrustes_data_backbone)


            corrected_mesoscopic_coordinates, distance, single_distances = calculate_distance_to_original_shape(
                clash_suite, complete_suites, meso_shift_shape, procrustes_data)
            neighbor_dihedral_angles = [dihedral_angles[i] for i in neighbors_cluster]

            torus_mean = [torus_mean_and_var(np.array(neighbor_dihedral_angles)[:, i].reshape((len(neighbor_dihedral_angles), 1)))[0][0] for i in range(np.array(neighbor_dihedral_angles).shape[1])]
            complete_suites[clash_suite].clean['torus mean'] = torus_mean
            complete_suites[clash_suite].clean['corrected mesoscopic shape'] = meso_shift_shape
            complete_suites[clash_suite].clean['corrected mesoscopic strand'] = corrected_mesoscopic_coordinates
            index_relative = np.argmax(nr_in_cluster_relative)
            if not index_relative == np.argmax(nr_in_cluster):
                neighbors_second_cluster = list(set(cluster_list_backbone[index_relative]) & set(neighbors))
                neighbor_dihedral_angles_second = [dihedral_angles[i] for i in neighbors_second_cluster]
                torus_mean_second = [torus_mean_and_var(np.array(neighbor_dihedral_angles_second)[:, i].reshape((len(neighbor_dihedral_angles_second), 1)))[0][0] for i in range(np.array(neighbor_dihedral_angles).shape[1])]
                complete_suites[clash_suite].clean['torus mean second'] = torus_mean_second
            else:
                complete_suites[clash_suite].clean['torus mean second'] = None
            all_single_distances.append(single_distances)
            all_distances.append(distance)
            distance_resolution = 0.5*distance/complete_suites[clash_suite].resolution
            all_distances_resolution.append(distance_resolution)
            all_resolutions.append(complete_suites[clash_suite].resolution)


            if distance_resolution < 0.5:
                best_string_distance = folder + 'resolution_0_05/'
            if distance_resolution >= 0.5 and distance_resolution < 1:
                best_string_distance = folder + 'resolution_05_1/'
            if distance_resolution >= 1 and distance_resolution < 1.5:
                best_string_distance = folder + 'resolution_1_15/'
            if distance_resolution >= 1.5:
                best_string_distance = folder + 'resolution_15_/'
            if not os.path.exists(best_string_distance):
                os.makedirs(best_string_distance)
            if len(complete_suites[clash_suite].erraser) > 0:
                erraser_corrections.append(np.mean(procrustes_data_backbone[neighbors_cluster], axis=0))
            if plot_all:
                plot_4_ring_correction(best_string_distance, clash_suite, complete_suites, meso_shift_shape, neighbors,
                                       neighbors_cluster, neighbour_mean, number_neighbors, procrustes_data,
                                       procrustes_data_backbone, corrected_mesoscopic_coordinates,
                                       mesoscopic_clash_shape_rotated, procrustes_data_small_rotated)

    plot_correction_results(all_distances, all_distances_resolution, all_nr_in_cluster, all_single_distances,
                            complete_suites, erraser_corrections, folder)
    return input_suites


def orthogonal_projection_on_M_c(clash_suite, cluster_list_backbone, neighbors, nr_in_cluster, procrustes_data,
                                 procrustes_data_backbone):
    """
    The Step 2 of the multiscale RNA backbone correction. We calculate the orthogonal_projection_on_M_c as described in
    the paper.
    :param clash_suite:
    :param cluster_list_backbone:
    :param neighbors:
    :param nr_in_cluster:
    :param procrustes_data:
    :param procrustes_data_backbone:
    :return:
    """
    cluster_nr = np.argmax(nr_in_cluster)
    neighbors_cluster = list(set(cluster_list_backbone[cluster_nr]) & set(neighbors))
    # Step 1: Calculate the Procrustes mean of the neighbour set:
    procrustes_data_small_rotated = procrustes_data.copy()
    procrustes_data_small_rotated[neighbors_cluster] = procrustes_algorithm_short(procrustes_data[neighbors_cluster])[0]
    neighbour_mean = np.mean(procrustes_data_small_rotated[neighbors_cluster], axis=0)
    neighbour_mean = neighbour_mean - np.mean(neighbour_mean, axis=0)
    # Step 2: Rotate the clash shape optimal to the Procrustes mean
    mesoscopic_clash_shape_copy = procrustes_data[clash_suite].copy()
    x = mesoscopic_clash_shape_copy
    z = neighbour_mean
    mean_distance_between_center_sugar_rings = np.mean(np.linalg.norm(procrustes_data_small_rotated[neighbors_cluster][:,3] - procrustes_data_small_rotated[neighbors_cluster][:,2], axis=1))
    backbone_mean = np.mean(procrustes_data_backbone[neighbors_cluster], axis=0)
    distance_to_z, x, y, y_bar = inner_orthogonal_projection(backbone_mean, x, z, mean_distance_between_center_sugar_rings)
    return y_bar, neighbors_cluster, neighbour_mean, x, procrustes_data_small_rotated


def inner_orthogonal_projection(backbone_mean, x, z, mean_distance_between_center_sugar_rings=None):
    """
    A help function of orthogonal_projection_on_M_c.
    :param backbone_mean:
    :param x:
    :param z:
    :return:
    """
    if mean_distance_between_center_sugar_rings is None:
        l_c = np.linalg.norm(backbone_mean[1] - backbone_mean[7])
    else:
        l_c = mean_distance_between_center_sugar_rings
    x = rotate_y_optimal_to_x(z, x)
    y = z.copy()
    distance = np.inf
    distance_to_z = []
    #counter = 0
    y_bar = y.copy()
    #while distance > 1e-5:
    #counter = counter + 1
    # step 2b
    y_old = y_bar.copy()

    y_bar = y.copy()
    alpha = 0.5 * (1 + l_c / np.linalg.norm(y[2] - y[3]))
    beta = 0.5 * (1 + np.linalg.norm(x[5] - x[0]) / np.linalg.norm(y[5] - y[0]))

    y_bar[0] = beta * y[0] + (1 - beta) * y[5]
    y_bar[5] = beta * y[5] + (1 - beta) * y[0]

    y_bar[2] = alpha * y[2] + (1 - alpha) * y[3]
    y_bar[3] = alpha * y[3] + (1 - alpha) * y[2]

    y_z = rotate_y_optimal_to_x(x=z, y=y_bar)
    y = 0.5 * (y_z + z)
    y = rotate_y_optimal_to_x(x=z, y=y)
    distance_to_z.append(np.linalg.norm(z - y_z))
    distance = np.linalg.norm(y_bar - y_old)
    return distance_to_z, x, y, y_bar


def calculate_distance_to_original_shape(clash_suite, complete_suites, meso_shift_shape, procrustes_data):
    """
    In this function we transform the corrected mesoscopic shape in the original coordinate system so that the first and
    last landmark of the corrected mesoscopic strand are equal with the first and last landmark with the mesoscopic
    clash strand. The Procrustes distance is also calculated and returned.
    :param clash_suite: An int: The index of the clash suite.
    :param complete_suites: A list. The list of all complete suites.
    :param meso_shift_shape: The corrected mesoscopic shape.
    :param procrustes_data: The procrustes data set.
    :return:
    """
    # First rotate such that the sixth and first landmark are superimposed
    procrustes_distance = np.linalg.norm(rotate_y_optimal_to_x(procrustes_data[clash_suite], meso_shift_shape) - procrustes_data[clash_suite])
    procrustes_distance_single = np.linalg.norm(rotate_y_optimal_to_x(procrustes_data[clash_suite], meso_shift_shape) - procrustes_data[clash_suite], axis=1)

    # Step 1:
    meso_shift_shape_shifted = meso_shift_shape - meso_shift_shape[0]
    procrustes_shifted = procrustes_data[clash_suite] - procrustes_data[clash_suite][0]

    # Step 2:
    first_rotation = rotation(meso_shift_shape_shifted[5]/np.linalg.norm(meso_shift_shape_shifted[5]), procrustes_shifted[5]/np.linalg.norm(procrustes_shifted[5]))
    meso_shift_shape_shifted_rotated = meso_shift_shape_shifted @ first_rotation.T

    # Step 3:
    alpha = leastsq(distance_rotation, x0=np.array([0.0]), args=(procrustes_shifted[5]/np.linalg.norm(procrustes_shifted[5]), meso_shift_shape_shifted_rotated,
                                                                 procrustes_shifted))
    meso_shift_shape_rotated_shifted = np.dot(meso_shift_shape_shifted_rotated, rotation_matrix_axis_angle(procrustes_shifted[5]/np.linalg.norm(procrustes_shifted[5]), alpha[0])).reshape(6, 3)
    meso_shift_shape_rotated_rotated = meso_shift_shape_rotated_shifted + procrustes_data[clash_suite][0]



    corrected_mesoscopic_coordinates = np.dot(complete_suites[clash_suite].procrustes_complete_mesoscopic_rotation.T, np.transpose(meso_shift_shape_rotated_rotated)) + complete_suites[clash_suite].procrustes_complete_mesoscopic_shift.reshape(3, 1)
    corrected_mesoscopic_coordinates = corrected_mesoscopic_coordinates.T

    return corrected_mesoscopic_coordinates, procrustes_distance, procrustes_distance_single


def create_cluster_list(complete_suites, string_):
    """
    This function is a help function to generate cluster lists.
    :param complete_suites: A list with all complete suites.
    :param string_: A string. The name of the clustering.
    :return: A list with all clusters.
    """
    a = [[suite_number, complete_suites[suite_number].clustering[string_]] for suite_number in
         range(len(complete_suites)) if string_ in complete_suites[suite_number].clustering]
    a_max = np.max([a[i][1] for i in range(len(a)) if a[i][1] is not None]) + 1
    cluster_list = [[] for i in range(a_max)]
    for i in range(len(a)):
        for j in range(a_max):
            if a[i][1] == j:
                cluster_list[j] = cluster_list[j] + [a[i][0]]
    return cluster_list




