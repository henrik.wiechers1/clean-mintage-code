The starting point for this gitlab is main_work_with_suites.py.
It executes the following functions one after the other:

* Step 1: Use the methods in parse_pdb_and_create_suites.py to create a list of objects of the Suite-type (see
Suite_class.py).
* Step 2: Use the methods in read_clash_files.py to extend the suites with information about atom-clashes.
* Step 3: Use the methods in shape_analysis to extend the suites with information about the shape. I.e. use the Procrustes algorithm to mean align the shapes and cluster the data.
* Step 4: Parse the ERRASER files. And make statistics with the ERRASER data.
* Step 5: Apply CLEAN (CLassification basEd on mutliscAle eNhancement).
* Step 6: Apply CLEAN to two examples of the Corona data set.

The the output and all results are plotted in the 'out' folder.
Tested on Ubuntu Linux with python 3.6, with numpy, scipy and matplotlib. The software should run without changes on other operating systems with a current (2020 or later) python 3 environment including the necessary libraries. 

This software is free software, licensed under the GNU GPL 3. For details see the file GPL3.txt.
