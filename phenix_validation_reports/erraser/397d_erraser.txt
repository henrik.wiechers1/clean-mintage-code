
Using electron cloud x-H distances and vdW radii

Adding H/D atoms with reduce...

Bad Clashes >= 0.4 Angstrom:
 B  36    G  OP1  B 204  HOH  O   :1.045
 B  36    G  P    B 204  HOH  O   :0.589
 B  44    G  OP1  B 143  HOH  O   :0.418
 A  29    C  O2'  A  30    G  H5' :0.411
clashscore = 4.59
