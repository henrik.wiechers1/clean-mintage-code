
Using electron cloud x-H distances and vdW radii

Adding H/D atoms with reduce...

Bad Clashes >= 0.4 Angstrom:
 A 127    A  O2'  A 128    G  OP2 :0.893
 A 128    G  O2'  A 129    A  OP2 :0.837
 A 126    A  O2'  A 127    A  OP2 :0.816
 A 128    G  O2'  A 129    A  P   :0.802
 A 134    G  O2'  A 135    C  H5' :0.794
 A 188    G  O2'  A 189    C  OP2 :0.792
 A 137    A H5''  A 138    G  OP1 :0.757
 A  79    G  O2'  A  80    C  OP1 :0.712
 A 219    C  C2'  A 220    A  OP2 :0.674
 A 108    A  O2'  A 109    C  P   :0.673
 A 133    C  H2'  A 134    G  H5' :0.664
 A 175    C  H2'  A 175    C  O2  :0.656
 A 219    C  O2'  A 220    A  OP2 :0.645
 A 220    A  O2'  A 221    C  OP1 :0.621
 A 165    A  O2'  A 166    A  P   :0.605
 A 159    G  O2'  A 160    C  OP2 :0.599
 A 159    G  C2'  A 160    C  OP2 :0.596
 A 127    A HO2'  A 128    G  P   :0.581
 A 126    A  H4'  A 127    A  O5' :0.576
 A 134    G  H2'  A 135    C  H6  :0.573
 A 128    G HO2'  A 129    A  P   :0.562
 A 129    A  C4   A 175    C  N4  :0.558
 A 127    A  O2'  A 128    G  P   :0.558
 A 188    G  H1'  A 189    C  OP2 :0.551
 A 133    C  C2'  A 134    G  H5' :0.538
 A 220    A  C2'  A 221    C  OP1 :0.537
 A 198    U  O2'  A 199    G  P   :0.531
 A 159    G  O2'  A 160    C  P   :0.527
 A 121    C  H2'  A 122    A  H5' :0.526
 A 188    G  C2'  A 189    C  OP2 :0.518
 A 198    U  H4'  A 199    G  OP1 :0.510
 A 126    A HO2'  A 127    A  P   :0.504
 A 133    C  H2'  A 134    G  C5' :0.497
 A 165    A  O2'  A 166    A  OP2 :0.491
 A 108    A HO2'  A 109    C  P   :0.485
 A 225    A  H4'  A 227    A  H5' :0.471
 A  76    G  C8   A  76    G  OP1 :0.469
 A 134    G  H2'  A 135    C  C6  :0.467
 A 137    A  N6   A 158    U  H3' :0.456
 A 121    C  C2'  A 122    A  H5' :0.455
 A 165    A  O2'  A 166    A H5'' :0.454
 A 175    C  C2'  A 175    C  O2  :0.454
 A 165    A  H4'  A 166    A  O5' :0.450
 A 102    A  H8   A 102    A  O5' :0.443
 A  82    A  H4'  A 206    C  O2  :0.442
 A 111    G  C2   A 230    C  C2  :0.429
 A 127    A  N6   A 222    G  C4  :0.427
 A 218    G  H4'  A 219    C  O5' :0.426
 A 198    U  O2'  A 199    G  O5' :0.425
 A 219    C  H2'  A 220    A  OP2 :0.422
 A 219    C  O2'  A 220    A  P   :0.414
 A 122    A  H2'  A 123    G  C5' :0.412
 A 108    A  O2'  A 109    C  OP2 :0.411
 A 111    G  O2'  A 112    A  H5' :0.405
 A 126    A  O2'  A 127    A  P   :0.405
 A 137    A  N6   A 159    G  OP2 :0.402
clashscore = 11.05
