OUTPUT_FOLDER = "out/"
RELEVANT_ATOMS = [" P  ", " O5*", " C5*", " C4*", " C3*", " O3*", " O4*",
                  " C1*", " N9 ", " C4 ", " N1 ", " C2 ", " C2*", " O2*"]
RELEVANT_ATOMS_BACKBONE_HYDROGEN_ATOMS = [" H5*", "H5**", " H4*", " H3*"]
RELEVANT_RING_ATOMS = [" O4*", " C1*", " C2*", " O2*"]
RELEVANT_RING_ATOMS_HYDROGEN = [" H1*", " H2*", "HO2*"]
RELEVANT_OXYGEN_ATOMS = [" OP1", " OP2"]
SUGAR_ATOMS = [" C4*", " C3*", " C2*", " C1*", " O4*"]
RELEVANT_ATOMS_ONE_RING = [" O4*", " C1*", " N1 ", " C2 "]
RELEVANT_ATOMS_TWO_RING = [" O4*", " C1*", " N9 ", " C4 "]
ONE_RING_BASES = ["  U", "  C", "  T"]
TWO_RING_BASES = ["  G", "  A"]
BASES = ONE_RING_BASES + TWO_RING_BASES
RNA_BASES_VALIDATION = ["U", "C", "G", "A"]
BASE_ATOMS_VALIDATION = ['N4', 'N2', 'C6', 'H3', 'C5', 'H22', 'N7', 'C2', 'N3', 'O2', 'H41', 'N9', 'O6',
                         'H8', 'H21', 'H5', 'N1', 'H2', 'H6', 'N6', 'H62', 'C8', 'H61', 'C4', 'O4', 'H42', 'H1']
RING_ATOMS_VALIDATION = ["O4'", "C1'", "H1'", "C2'", "H2'", "O2'", "HO2'"]
BACKBONE_ATOMS_VALIDATION = ["C4'", "H4'", "C3'", "H3'", "O3'", 'P', "O5'", "C5'", "H5'", "H5''", 'OP1', 'OP2']

COLORS = ['black', 'darkred', 'darkgreen', 'darkmagenta', 'royalblue', 'grey', 'pink', 'yellow', 'teal', 'gold', 'navy',
          'magenta', 'darkviolet', 'tomato', 'peru', 'darkkhaki', 'darkslategray', 'springgreen'] + ['black'] * 1000

COLORS_SCATTER = ['black', 'darkred', 'pink', 'teal', 'darkgreen', 'grey', 'darkmagenta', 'royalblue', 'navy',
                  'gold', 'yellow', 'darkkhaki', 'darkviolet', 'tomato', 'peru', 'springgreen', 'magenta',  'darkslategray'] + ['black'] * 1000
MARKERS = ['.', 'p', 's', '*', 'd', 'D', 'P', 'p', '^', '<', '>', 'X', 'o', 'v', '8'] + ['p']*1000
#MARKERS = ['o'] * 10