import os
import pickle
from scipy.cluster.hierarchy import average, fcluster, single, ward, centroid, weighted
from plot_functions import build_fancy_chain_plot, hist_own_plot
import numpy as np
import csv
import collections
import PNDS_RNA_clustering
from scipy.spatial.distance import pdist
from data_functions import procrustes_algorithm_short, mean_on_sphere
from constants import COLORS, COLORS_SCATTER
from help_plot_functions import plot_clustering
from PNDS_PNS import torus_mean_and_var


def procrustes_on_suite_class(np_array, string, string_plot):
    """
    This method is a help function. It checks if the Procrustes algorithm has already been calculated and in this case
    loads the corresponding file. Otherwise the procrustes algorithm is called.
    :param np_array: The array of pre shapes.
    :param string: The name of the fild where the result of the Procrustes algorithm is stored
    :param string_plot: The name of the folder where the plots of the Procrustes algorithm is stored.
    :return:
    """
    print('procrustes_on_suite_class', string_plot)
    if os.path.isfile(string):
        with open(string, 'rb') as f:
            procrustes_complete_suites = pickle.load(f)
    else:
        procrustes_data, shift_array, scale_array, rotation_matrices = procrustes_algorithm_short(
            np_array, string_plot)
        procrustes_complete_suites = [procrustes_data, shift_array, scale_array, rotation_matrices]
        with open(string, 'wb') as f:
            pickle.dump(procrustes_complete_suites, f)
    return procrustes_complete_suites


def procrustes_analysis(suites):
    """
    A function that assigns procrustes attributes to suite objects.
    :param suites: A list of suites.
    :return:
    """
    complete_suites = [suite for suite in suites if suite.complete_suite]
    complete_suites_bb_atoms = np.array([suite.backbone_atoms for suite in complete_suites])
    string = './out/procrustes/suites_complete_size_shape.pickle'
    string_plot = './out/procrustes/suites'
    print(string_plot)
    procrustes_complete_suites = procrustes_on_suite_class(complete_suites_bb_atoms, string, string_plot)

    for i in range(len(complete_suites)):
        complete_suites[i].procrustes_complete_suite_vector = procrustes_complete_suites[0][i]
        complete_suites[i].procrustes_complete_suite_shift = procrustes_complete_suites[1][i]
        complete_suites[i].procrustes_complete_suite_scale = procrustes_complete_suites[2][i]
        complete_suites[i].procrustes_complete_suite_rotation = procrustes_complete_suites[3][i]

    complete_mesoscopic_rings = np.array([suite.mesoscopic_sugar_rings for suite in complete_suites])
    string = './out/procrustes/mesoscopic_complete_size_shape.pickle'
    string_plot = './out/procrustes/mesoscopic'
    procrustes_complete_mesoscopic = procrustes_on_suite_class(complete_mesoscopic_rings, string, string_plot)
    for i in range(len(complete_suites)):
        complete_suites[i].procrustes_complete_mesoscopic_vector = procrustes_complete_mesoscopic[0][i]
        complete_suites[i].procrustes_complete_mesoscopic_shift = procrustes_complete_mesoscopic[1][i]
        complete_suites[i].procrustes_complete_mesoscopic_scale = procrustes_complete_mesoscopic[2][i]
        complete_suites[i].procrustes_complete_mesoscopic_rotation = procrustes_complete_mesoscopic[3][i]
    return suites


def average_clustering(input_suites, m, percentage, clean, plot=True):
    """
    A function that clusters the mesoscopics using the classical average clustering.
    :param input_suites:
    :param m:
    :param percentage:
    :param clean:
    :param plot:
    :return:
    """
    method = average
    string = './out/average_mesoscopic_clustering/' + 'size_and_shape_m' + str(m) + 'percentage_' + str(percentage) + '/'
    if not os.path.exists(string):
        os.makedirs(string)
    cluster_suites = [suite for suite in input_suites if suite.complete_suite]
    if clean:
        cluster_suites = [suite for suite in cluster_suites if len(suite.bb_bb_neighbour_clashes) == 0
                          and len(suite.bb_bb_one_suite) == 0]
    procrustes_data = np.array([suite.procrustes_complete_mesoscopic_vector for suite in cluster_suites])
    procrustes_data_backbone = np.array([suite.procrustes_complete_suite_vector for suite in cluster_suites])
    distance_data = pdist(procrustes_data.reshape(procrustes_data.shape[0], procrustes_data.shape[1] * 3))
    cluster_data = method(distance_data)
    threshold = find_outlier_threshold(cluster=cluster_data, percentage=percentage, input_data_shape_0=procrustes_data.shape[0], m=1)
    f_cluster = fcluster(cluster_data, threshold, criterion='distance')
    outlier_cluster_index = [i for i in range(1, max(f_cluster) + 1) if sum(f_cluster == i) <= 1]
    biggest_cluster_information = np.array(collections.Counter(iter(f_cluster)).most_common(np.max(f_cluster)))
    outlier_list = [j for j in range(procrustes_data.shape[0]) if f_cluster[j] in outlier_cluster_index]
    cluster_list = []
    biggest_cluster_information = np.array(collections.Counter(iter(f_cluster)).most_common(np.max(f_cluster)))
    for i in range(biggest_cluster_information.shape[0]):
        if biggest_cluster_information[i, 1] > m: #and biggest_cluster_information[i, 1] <= m:
            index_list = [k for k in range(procrustes_data.shape[0]) if f_cluster[k] == biggest_cluster_information[i, 0]]
            cluster_list = cluster_list + [index_list]

    cluster_numbers = [suite.clustering['suite_True'] for suite in cluster_suites]
    cluster_number_list = [[cluster_numbers[cluster_list[i][j]] for j in range(len(cluster_list[i]))] for i in range(len(cluster_list))]
    cluster_cut = [collections.Counter(cluster_number_list[i]).most_common(np.max(1))[0][1]/len(cluster_number_list[i])*100 for i in range(len(cluster_number_list))]

    hist_own_plot(cluster_cut, x_label='percentages of single MINT-AGE classes', y_label='number of simple mesoscopic clusters', filename=string+'hist_percentages', bins=None, density=False)
    dihedral_angles = [suite.dihedral_angles*np.pi/180-np.pi for suite in cluster_suites]
    var_different_suite_cluster = [np.sqrt(torus_mean_and_var(np.array(dihedral_angles)[cluster_list[i]])[1]) for i in range(len(cluster_list))]

    hist_own_plot(var_different_suite_cluster, x_label='torus standard deviation', y_label='number of simple mesoscopic clusters', filename=string+'hist', bins=None, density=False)
    print('test')
    plot_all = True
    if plot_all:
        i=0
        j=29
        #k=43
        l=54
        m=91
        colors = [COLORS[0]]*len(cluster_list[i]) + [COLORS[2]]*len(cluster_list[j])  +['magenta']*len(cluster_list[l]) +  [COLORS[1]]*len(cluster_list[m])
        build_fancy_chain_plot(procrustes_data_backbone[list(cluster_list[i]) + list(cluster_list[j])  + list(cluster_list[l])+ list(cluster_list[m])],
                               filename=string + 'cluster_nr' + str(i) + 'and' + str(j) +  'and' + str(l) + 'and' + str(m)+ '_suite',
                               colors=colors,
                               create_label=False, alpha_line_vec=[1]*len(cluster_list[i]) + [1]*len(cluster_list[j]) + [1]*len(cluster_list[l])+ [1]*len(cluster_list[m]),
                               plot_atoms=True, atom_alpha_vector=[1]*len(cluster_list[i]) + [1]*len(cluster_list[j]) + [1]*len(cluster_list[l])+ [1]*len(cluster_list[m]),
                               atom_color_vector=colors, atom_size=0.5, lw=0.4)

        build_fancy_chain_plot(procrustes_data[list(cluster_list[i]) + list(cluster_list[j]) + list(cluster_list[l])+ list(cluster_list[m])],
                               filename=string + 'cluster_nr' + str(i) + 'and' + str(j) + 'and' + str(l) + 'and' + str(m)+ '_meso',
                               colors=colors,
                               create_label=False, alpha_line_vec=[1]*len(cluster_list[i]) + [1]*len(cluster_list[j])  + [1]*len(cluster_list[l])+ [1]*len(cluster_list[m]),
                               plot_atoms=True, atom_alpha_vector=[1]*len(cluster_list[i]) + [1]*len(cluster_list[j])  + [1]*len(cluster_list[l])+ [1]*len(cluster_list[m]),
                               atom_color_vector=colors, atom_size=0.5, lw=0.4)

    plot_clustering(cluster_suites, name=string, cluster_list=cluster_list, outlier_list=outlier_list)


def branch_cutting_with_correction(input_suites, m, percentage, clustering, clean, q_fold, plot=True):
    """
    First Step: Pre clustering.
    Second: Using Mode Hunting and Torus PCA to post cluster the data
    :param input_suites: A list with suite objects.
    :param m: An integer. The minimum cluster size.
    :param percentage: A float. The maximal outlier distance is described by the percentage of elements in a branch with
                       less than m elements.
    :param clustering: A string in ['suite', 'mesoscopic', 'combination_mesoscopic_suite'].
    :param clean: Boolean. If True: Clustering of the training data. If False: Clustering of the admissible data.
    :param q_fold: A float. The percentage value of the q_fold described in the paper.
    :param plot: If True: Plot all figures.
    """
    # Step 1: Pre-Clustering
    method = average
    if clean:
        string = './out/clustering/training_suites/m_is_' + str(m) + 'percentage_is_' + str(percentage) + 'q_fold_is' + str(q_fold) + '/'
    else:
        string = './out/clustering/admissible_suites/m_is_' + str(m) + 'percentage_is_' + str(percentage) + 'q_fold_is' + str(q_fold) + '/'
    cluster_suites = [suite for suite in input_suites if suite.complete_suite]
    if clean:
        cluster_suites = [suite for suite in cluster_suites if len(suite.bb_bb_neighbour_clashes) == 0
                          and len(suite.bb_bb_one_suite) == 0]
    # clean_suites = [suite for suite in complete_suites if len(suite.clash_list) == 0] #  suites with no clash
    if clustering == 'suite':
        procrustes_data = np.array([suite.procrustes_complete_suite_vector for suite in cluster_suites])
    if clustering == 'mesoscopic':
        procrustes_data = np.array([suite.procrustes_complete_mesoscopic_vector for suite in cluster_suites])
    if clustering == 'combination_mesoscopic_suite':
        procrustes_data = np.array([np.vstack((suite.procrustes_complete_mesoscopic_vector,
                                               suite.procrustes_complete_suite_vector)) for suite in cluster_suites])



    if clean:
        dihedral_angles_suites = np.array([suite._dihedral_angles for suite in cluster_suites if len(suite.bb_bb_one_suite) == 0 and len(suite.bb_bb_neighbour_clashes) == 0])
    else:
        dihedral_angles_suites = np.array([suite._dihedral_angles for suite in cluster_suites])

    cluster_list, outlier_list, name = pre_clustering(input_data=dihedral_angles_suites, m=m,
                                                      percentage=percentage,
                                                      string_folder=string, method=method,
                                                      q_fold=q_fold)


    if plot:
        plot_clustering(cluster_suites, name=name, cluster_list=cluster_list, outlier_list=outlier_list)

    # Step 2: sing Mode Hunting and Torus PCA to post cluster the data.
    cluster_list, noise = PNDS_RNA_clustering.new_multi_slink(scale=12000, data=dihedral_angles_suites, cluster_list=cluster_list, outlier_list=outlier_list)
    outlier_list = [i for i in range(procrustes_data.shape[0]) if i not in [cluster_element for cluster in cluster_list for cluster_element in cluster]]
    if plot:
        plot_clustering(cluster_suites, name=name + 'Torus_PCA/', cluster_list=cluster_list, outlier_list=outlier_list)#, plot_combinations=True, dihedral_angles_suites=dihedral_angles_suites)
        #plot_clustering(cluster_suites, name=name + 'Torus_PCA_noise/', cluster_list=noise, outlier_list=outlier_list)

    #cluster_list, outlier_list, name = cluster_cutting(procrustes_data=procrustes_data, cluster_list=cluster_list, m=m,
                                                       #name=name)

    # if plot:
    #     plot_clustering(cluster_suites, name=name, cluster_list=cluster_list, outlier_list=outlier_list)

    for suite_number in range(len(cluster_suites)):
        cluster_suites[suite_number].clustering[clustering + '_' + str(clean)] = None
        for cluster_number in range(len(cluster_list)):
            if suite_number in cluster_list[cluster_number]:
                cluster_suites[suite_number].clustering[clustering + '_' + str(clean)] = cluster_number
    return input_suites


def pre_clustering(input_data, m, percentage, string_folder, method, q_fold):
    """
    The pre clustering described in the paper.
    :param input_data: A np.array with the shape
    :param m: The minimal cluster size.
    :param percentage: A float value. It describes the maximal outlier distance.
    :param string_folder: The name of the folder where the plots are stored.
    :param method: A fuction (average, single, ward, ... ) from scipy.cluster.hierarchy.
    :param q_fold: A float value (introduced in the paper).
    :return:
    """
    index_map = {str(input_data[i]): i for i in range(input_data.shape[0])}
    # sp_number = np.sqrt(50*input_data.shape[0])
    # print(sp_number)
    n = input_data.shape[0]
    dimension_number = input_data.shape[1]
    cluster_points = input_data.copy()
    reshape_data = input_data
    distance_data = distance_matrix(reshape_data)
    cluster_data = method(distance_data)
    d_max = find_outlier_threshold(cluster=cluster_data, percentage=percentage, input_data_shape_0=n, m=m)
    # Step 1:
    outlier_list = []
    cluster_list = []
    counter = 0
    while n > 0:
        # Step 2:
        points_reshape = cluster_points.reshape(n, dimension_number)
        distance_points = distance_matrix(points_reshape)
        cluster_tree = method(distance_points)
        # Step 3:
        f_cluster = fcluster(cluster_tree, d_max, criterion='distance')
        outlier_cluster_nr_list = [i for i in range(1, max(f_cluster) + 1) if sum(f_cluster == i) < m]
        outlier_sub_list = [cluster_points[i] for i in range(n) if f_cluster[i] in outlier_cluster_nr_list]
        not_outlier_sub_list_number = [i for i in range(n) if not f_cluster[i] in outlier_cluster_nr_list]
        cluster_points = cluster_points[not_outlier_sub_list_number]
        n = cluster_points.shape[0]
        print(n)
        if n > 0:
            outlier_list = outlier_list + outlier_sub_list
            # Step 4:
            s_p = np.sqrt(n + m ** 2)
            print(s_p)
            points_reshape = cluster_points.reshape(n, dimension_number)
            distance_points = distance_matrix(points_reshape)
            cluster_tree = method(distance_points)
            # Step 5:
            sub_cluster_list, sub_list = branch_cutting(cluster_tree, cluster_points, s_p, q_fold)
            cluster_list = cluster_list + [cluster_points[sub_cluster_list]]
            cluster_points = cluster_points[sub_list]
            n = cluster_points.shape[0]
            if not os.path.exists(string_folder):
                os.makedirs(string_folder)
            counter = counter + 1

    cluster_index = [[index_map[str(cluster_list[j][i])] for i in range(cluster_list[j].shape[0])] for j in
                     range(len(cluster_list))]
    outlier_index = [index_map[str(outlier_list[i])] for i in range(len(outlier_list))]
    return cluster_index, outlier_index, string_folder


def distance_matrix(dihedral_angles):
    """This function calculates a distance vector that can be used by scipy.cluster.hierarchy's agglomerative clustering
     algorithms. The coordinates of a point are the lines of [dihedral_angles, shape_angles, scaled_distances].
     The distance is the euclidean distance, whereas the euclidean torus distance is taken for the dihedral_angles.
    :param dihedral_angles: A data matrix with the dimension (number of shapes) x 7.
    :return: A distance vector with the length(number of shapes)*(number of shapes-1)/2.
    """
    sum_dihedral_differences = np.zeros(np.int(dihedral_angles.shape[0] * (dihedral_angles.shape[0] - 1) / 2))
    for i in range(dihedral_angles.shape[1]):
        diff_one_dim = pdist(dihedral_angles[:, i].reshape((dihedral_angles.shape[0], 1)))
        #diff_one_dim = np.min((2*np.pi - diff_one_dim, diff_one_dim), axis=0) ** 2
        diff_one_dim = np.min((360 - diff_one_dim, diff_one_dim), axis=0) ** 2
        sum_dihedral_differences = sum_dihedral_differences + diff_one_dim
    return np.sqrt(sum_dihedral_differences)


def circular_mean(data):
    """
    This function gets da data vector with values between 0 and 2*pi and returns the mean and the variance corresponding
    to the torus metric. It uses the function variances.
    :param data: A one-dimensional vector with values between 0 and 2pi.
    :return: A list with [mean(data), var(data)].
    """
    n = len(data)
    mean0 = np.mean(data)
    var0 = np.var(data)
    sorted_points = np.sort(data)
    candidates = variances(mean0, var0, n, sorted_points)
    candidates[:, 0] = (candidates[:, 0] + (2 * np.pi)) % (2 * np.pi)
    # Use the unbiased estimator:
    candidates[:, 1] = candidates[:, 1] * n / (n - 1)
    return candidates[np.argmin(candidates[:, 1])]


def variances(mean0, var0, n, points):
    """
    This function is a help function for the function circular_mean.
    :param mean0: The mean of the data (not regarding the torus metric see circular_mean).
    :param var0: The var of the data (not regarding the torus metric see circular_mean).
    :param n: The length of the data (see circular_mean).
    :param points: The sorted data (see circular_mean).
    :return: A list of candidates [[mean_1, var_1],...,[mean_n, var_n]]
    """
    means = (mean0 + np.linspace(0, 2 * np.pi, n, endpoint=False)) % (2 * np.pi)
    means[means >= np.pi] -= 2 * np.pi
    parts = [(sum(points) / n) if means[0] < 0 else 0]
    parts += [((sum(points[:i]) / i) if means[i] >= 0 else (sum(points[i:]) / (n - i)))
              for i in range(1, len(means))]
    # Formula (6) from Hotz, Huckemann:
    means = [[means[i],
              var0 + (0 if i == 0 else
                      ((4 * np.pi * i / n) * (np.pi + parts[i] - mean0) -
                       (2 * np.pi * i / n) ** 2) if means[i] >= 0 else
                      ((4 * np.pi * (n - i) / n) * (np.pi - parts[i] + mean0) -
                       (2 * np.pi * (n - i) / n) ** 2))]
             for i in range(len(means))]
    return np.array(means)


def find_outlier_threshold(cluster, percentage, input_data_shape_0, counter_step_size=30, percentage_threshold=0.04, m=1):
    """
    This function returns for a given percentage value and given linkage matrix the corresponding threshold value such
    that fcluster(cluster, threshold, criterion='distance') has percentage outliers.
    :param cluster: A linkage matrix.
    :param percentage: A float value between 0 and 1.
    :param input_data_shape_0: The number of elements in the data set.
    :param counter_step_size: The size of the steps in the while loop.
    :param percentage_threshold: A value between 0 and 1.
    :return:
    """
    percentage_outliers = 1
    counter = 0
    while percentage_outliers > percentage:
        threshold = cluster[counter, 2]
        f_cluster = fcluster(cluster, threshold, criterion='distance')
        cluster_information = collections.Counter(iter(f_cluster))
        outlier_number= np.array([cluster_information[i] for i in cluster_information if cluster_information[i] <= m])
        percentage_outliers = sum(outlier_number) / input_data_shape_0
        if np.abs(percentage_outliers - percentage) > percentage_threshold:
            counter = counter + counter_step_size
        else:
            counter = counter + 1
    return threshold


def branch_cutting(cluster_tree, cluster_points, s_p, q_fold):
    """
    The branch cutting step from the pre clustering as described in the paper.
    :param cluster_tree:
    :param cluster_points:
    :param s_p:
    :param q_fold:
    :return:
    """
    help_list = []
    help_list_tuple = []
    true_value = True
    biggest_cluster_point = cluster_tree.shape[0] - 1
    while true_value:
        first_value = np.int(np.min(cluster_tree[biggest_cluster_point, :2])) - cluster_points.shape[0]
        second_value = np.int(np.max(cluster_tree[biggest_cluster_point, :2])) - cluster_points.shape[0]
        # print(first_value)
        if first_value > -1:
            first_cluster_size = int(cluster_tree[first_value, 3])
            second_cluster_size = int(cluster_tree[second_value, 3])
            if first_cluster_size < second_cluster_size:
                smaller_cluster = first_value
                bigger_cluster = second_value
            else:
                smaller_cluster = second_value
                bigger_cluster = first_value
            help_list_tuple = help_list_tuple + [[smaller_cluster, bigger_cluster]]
            if int(cluster_tree[smaller_cluster, 3]) > s_p and ((1-cluster_tree[bigger_cluster, 2]/cluster_tree[biggest_cluster_point, 2] > q_fold) and (1-cluster_tree[smaller_cluster, 2]/cluster_tree[biggest_cluster_point, 2] > q_fold)):
                help_list = help_list + [smaller_cluster]
            biggest_cluster_point = bigger_cluster
        else:
            if second_value < 0:
                true_value = False
            else:
                biggest_cluster_point = second_value
    if len(help_list) < 1:
        return [i for i in range(cluster_points.shape[0])], []
    last_smaller_cluster = help_list[len(help_list) - 1]
    for i in range(len(help_list_tuple)):
        if help_list_tuple[i][0] == last_smaller_cluster:
            help_list = help_list + [help_list_tuple[i][1]]

    biggest_cluster_in_l = help_list[np.argmax([np.int(cluster_tree[int_][3]) for int_ in help_list])]

    one_element = return_one_element(cluster_tree, biggest_cluster_in_l)
    f_cluster = fcluster(cluster_tree, cluster_tree[biggest_cluster_in_l, 2], criterion='distance')
    return [i for i in range(cluster_points.shape[0]) if f_cluster[i] == f_cluster[one_element]], \
           [i for i in range(cluster_points.shape[0]) if not f_cluster[i] == f_cluster[one_element]]


def return_one_element(cluster_tree, biggest_cluster_in_l):
    """
    A help function for the branch cutting algorithm.
    :param cluster_tree:
    :param biggest_cluster_in_l:
    :return:
    """
    run_value = biggest_cluster_in_l
    # first_value = np.int(np.min(cluster_tree[run_value, :2]))-cluster_tree.shape[0]  - 1

    # second_value = np.int(np.max(cluster_tree[biggest_cluster_in_l, :2]))-cluster_tree.shape[0] - 1
    true_value = True
    while true_value:
        first_value = np.int(np.min(cluster_tree[run_value, :2])) - cluster_tree.shape[0] - 1
        if first_value < 0:
            return first_value + cluster_tree.shape[0] + 1
        run_value = first_value